# Creating Spring Boot and React CRUD Full Stack Application with Maven

## Things to do:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-react-fullstack-examples.git`.
2. Go to each folder: `cd react-backend` for backend and `cd react-frontend` for frontend.
3. In `react-backend` folder run this command: `npm start`.
4. Navigate to your favorite browser: http://localhost:3000
5. In `react-backend` folder run this command: `mvn clean spring-boot:run`.

## Screen shot

**List Courses**

![List Courses](img/list.png "List Courses")

**Add New Course**

![Add New Course](img/add.png "Add New Course")

**Edit Course Data**

![Edit Course Data](img/edit.png "Edit Course Data")

**Delete Course Data**

![Delete Course Data](img/delete.png "Delete Course Data")

Please refer this [link](https://dzone.com/articles/creating-spring-boot-and-react-crud-full-stack-app?edition=521350&utm_source=Daily%20Digest&utm_medium=email&utm_campaign=Daily%20Digest%202019-09-30) .